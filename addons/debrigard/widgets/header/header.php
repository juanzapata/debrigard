<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 

class Widget_header extends Widgets

{

    // The widget title,  this is displayed in the admin interface

    public $title = array(        

        'en' => 'header(Widget)',

        'es' => 'header(Widget)',

    );

    public $description = array(        

        'en' => 'headerr.',

        'es' => 'header.',

    );

 

    // The author's name

    public $author = '';

 

    // The authors website for the widget

    public $website = 'www.imaginamos.com';

 

    //current version of your widget

    public $version = '1.0';

	

    public function run()

    {

        $this->db->order_by("id", "desc"); 

        $this->db->limit(10);

    	$header = $this->db->get($this->db->dbprefix.'header')

        ->result_array();

    	return array('header' => $header);

    }

}