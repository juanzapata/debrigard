<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 

class Widget_producto extends Widgets

{

    // The widget title,  this is displayed in the admin interface

    public $title = array(       

        'en' => 'product(Widget)',

        'es' => 'producto(Widget)'

    );

    public $description = array(        

        'en' => 'product.',

        'es' => 'producto.'

    );

 

    // The author's name

    public $author = '';

 

    // The authors website for the widget

    public $website = 'www.imaginamos.com';

 

    //current version of your widget

    public $version = '1.0';

	

    public function run()

    {

        $this->db->order_by("id", "desc"); 

        $this->db->limit(2);

        $this->db->where("estado",1);

    	$producto = $this->db->get($this->db->dbprefix.'products')

        ->result_array();

    	return array('producto' => $producto);

    }

}