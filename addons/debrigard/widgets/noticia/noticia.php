<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

 

class Widget_noticia extends Widgets

{

    // The widget title,  this is displayed in the admin interface

    public $title = array( 

    'en' => 'news(Widget)',      

        'es' => 'noticia(Widget)'

    );

    public $description = array(  

    'en' => 'news.',     

        'es' => 'noticia.'

    );

 

    // The author's name

    public $author = '';

 

    // The authors website for the widget

    public $website = 'www.imaginamos.com';

 

    //current version of your widget

    public $version = '1.0';

	

    public function run()

    {       

        $this->db->order_by("id", "desc"); 

        $this->db->limit(3);

        $this->db->where("estado",1);

    	$noticia = $this->db->get($this->db->dbprefix.'news')

        ->result_array();

    	return array('noticia' => $noticia);

    }

}