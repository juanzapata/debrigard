<div class="container">
	<!-- CARROUSEL -->
	<div id="carousel-example-generic" class="carousel slide " data-ride="carousel" style="max-width: 1080px;">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			{{banner}}
			<li data-target="#carousel-example-generic" data-slide-to="{{helper:count}}" class="{{ if { helper:count identifier='iut' } == 1 }}active{{ endif }}"></li>
			{{/banner}}
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			{{ banner }}
			<div class="item {{ if { helper:count identifier='asd' } == 1 }}active{{ endif }}">
				<a href="{{link}}"><img src="{{image}}" alt="{{title}}"></a>
				<div class="carousel-caption">
					<h3>{{title}}</h3>
					<p class="hidden-xs">{{text}}</p>
				</div>
			</div>
			{{ /banner }}
		</div>


		<!-- Controls -->
		<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		</a>
		<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		</a>
	</div>
	<div class="push"></div>
	
	<hr>
	<h4>Noticias o Publicaciones Destacadas</h4>
	<!-- Noticias Destacadas -->
	
	<!-- <div class="row">
	{{informative}}
		<div class="col-sm-6 col-md-3">			
				
					<h4>{{title}}</h4>
					<p>{{text}}</h4>	
				
			</div>
			{{/informative}} -->
		{{ widgets:area slug="noticias" }}
	</div>
	<div class="push"></div>
	<!-- <hr>
	<h4>Productos o Servicios Destacados</h4>

	{{ widgets:area slug="pro" }}
	<div class="row">
		
	</div> -->

	<!-- <div class="push"></div>
	<hr>
	<h4>Nuestros Clientes</h4>
	<!-- Slider neustros clientes -->
	<!-- <div class="row">
		{{outstanding_services}}
		<div class="col-sm-6 col-md-3">
			<div class="thumbnail">
				<div style="overflow: hidden;max-height:170px;">
					<a href="{{link}}" ><img src="{{image}}" alt="{{title}}" data-src="holder.js/300x200" class="img-responsive" style="min-width: 100%;"></a>
				</div>
				<div class="caption">
					<!--<h4>{{title}}</h4>
					<p>{{text}}</p>
					<p><a href="{{link}}" class="btn btn-primary btn-sm">Ver Mas</a></p>-
				</div>
			</div>
		</div>
		{{/outstanding_services}}
	</div> --> 
</div>