<?php



if (!defined('BASEPATH'))

    exit('No direct script access allowed');



/*

*

* @author 	    Brayan Acebo

* @package 	PyroCMS

* @subpackage 	Products

* @category 	Modulos

*/



class Faq extends Public_Controller {



    public function __construct() {

        parent::__construct();

        $this->load->model('faq_m');

    }





// -----------------------------------------------------------------



     public function index($lang="")

    {

          if(empty($lang)){



            $this->db->from($this->db->dbprefix('faq'));

            $query = $this->db->get();

            $solucion = $query->result();

            $projects = (array) $solucion;

            

            $_SESSION['lang']="es";

            $_SESSION['controlador']="faq";



            $this->lang->load('team','spanish');



        }else if($lang=="es"){



            $this->db->from($this->db->dbprefix('faq'));

            $query = $this->db->get();

            $solucion = $query->result();

            $projects = (array) $solucion;



            $_SESSION['lang']="es";

            $_SESSION['controlador']="faq";



            $this->lang->load('team','spanish');



        }

         $faq = $this->faq_m->get_all();

        $data['faq'] = $projects;

   

    	// Banner

       



        $pagination = create_pagination('faq/index', $this->faq_m->count_all(), 6, 3);



        $faq = $this->faq_m

        ->limit($pagination['limit'], $pagination['offset'])

        ->order_by('position','ASC')

        ->get_all();



        foreach($faq AS $item)

        {

            $item->pregunta = substr($item->pregunta, 0, 300);

            $item->respuesta = substr($item->respuesta, 0,300);
            

        }



        $this->template

        ->set('faq', $faq)

        ->set('pagination', $pagination['links'])

        ->build('index');

    }



    // ----------------------------------------------------------------------



    

}