<section class="item">

    <div class="content">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-view"><span>Encabezado</span></a></li>

                <li><a href="#page-details"><span>Administrar Contenido</span></a></li>



            </ul>

            <div class="form_inputs" id="page-view">

                <fieldset>

                    <ul>
                        
                       <!--  <li>

                            <label for="name">Telefono</label>

                            <div class="input"><?php echo isset($data->telephone) ? $data->telephone : "" ?></div>

                        </li> -->
                        <li>

                            <label for="name">Logo</label>

                            <div class="input"><?php echo isset($data->image) ? $data->image : "" ?></div>



                            <?php if (!empty($data->image)): ?>

                                <img src="<?php echo site_url($data->image); ?>" style="height: 50px;">

                            <?php endif; ?>
                        </li>

                    </ul>

                </fieldset>

            </div>

            <div class="form_inputs" id="page-details">

                <?php echo form_open_multipart(site_url('admin/header/'.$funcion.'/'), 'class="crud"'); ?>

                <div class="inline-form">

                    <fieldset>

                        <ul>
                           
                          <!--   <li>

                                <label for="name">telefono</label>

                                <div class="input"><?php echo form_input('telephone', set_value('telephone', isset($data->telephone) ? $data->telephone : ""), ' id="telephone"'); ?></div>

                            </li>   -->            


                            <li>

                                <label for="name">Imagen

                                    <small>

                                        - Imagen Permitidas gif | jpg | png | jpeg<br>

                                        - Tamaño Máximo 2 MB<br>

                                        - Ancho Máximo 400px<br>

                                        - Alto Máximo 400px

                                    </small>

                                </label>

                                <div class="input">

                                    <div class="btn-false">

                                        <div class="btn">Examinar</div>

                                        <?php echo form_upload('image', '', ' id="image"'); ?>

                                    </div>

                                </div>

                                <br class="clear">

                            </li>
                            <li>

                                <div class="buttons float-right padding-top">

                                    <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>

                                </div>

                            </li>



                        </ul>

                    </fieldset>

                </div>

                <?php echo form_close(); ?>

            </div>



            </section>



            <script>

                jQuery(function($) {

                    // generate a slug when the user types a title in

                    pyro.generate_slug('input[name="name"]', 'input[name="slug"]');



                });

            </script>

