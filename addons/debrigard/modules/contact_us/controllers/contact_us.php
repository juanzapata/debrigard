<?php



defined('BASEPATH') OR exit('No direct script access allowed');



/**

 * @author Christian España

 */

class Contact_Us extends Public_Controller {



    public function __construct() {

        parent::__construct();
         $models = array('contact_us_m', 'contact_us_emails_m');

        $this->load->model($models);

    }



    // -----------------------------------------------------------------



    public function index()

    {

        // Datos de Contacto

        $_contact_us = $this->contact_us_m->get_all();

        $contact_us = array();

        if (count($_contact_us) > 0) {

            $contact_us = $_contact_us[0];

        }



        $this->template

                ->set('contact_us', $contact_us)

                ->build('contact_us_front');

    }


    /*
     *Enviar correo
     */

    function send()

    {
        $_contact_us = $this->contact_us_m->get_all();

        $contact_us = array();

        if (count($_contact_us) > 0) {

            $contact_us = $_contact_us[0];

        }
       $post = (object) $this->input->post(null);



        $this->form_validation->set_rules('name', 'Nombre y Apellido', 'required|trim|max_length[100]');

        $this->form_validation->set_rules('email', 'Correo', 'required|trim|valid_email|max_length[100]');

        $this->form_validation->set_rules('phone', 'Teléfono', 'trim|max_length[30]');
        
        $this->form_validation->set_rules('cell', 'Celular', 'trim|max_length[30]');

        $this->form_validation->set_rules('company', 'Empresa/Organización', 'trim|max_length[100]');

        $this->form_validation->set_rules('city', 'Municipio/Departamento', 'trim|max_length[100]');

        $this->form_validation->set_rules('message', 'Mensaje', 'required|trim|max_length[455]');



        if ($this->form_validation->run() === TRUE) {



            $config = array(

                'mailtype' => 'html',

                'wordwrap' => TRUE,

                'protocol' => 'sendmail',

                'charset' => 'utf-8',

                'crlf' => '\r\n',

                'newline' => '\r\n'

            );



            $data['post'] = array(

                'name' => $post->name,

                'email' => $post->email,

                'phone' => $post->phone,
                
                'cell' => $post->cell,

                'company' => $post->company,

                'message' => $post->message,

            );

			

			// mandamos los datos a la Base de Datos

			$this->contact_us_emails_m->insert($data['post']);

			

			// mandamos los datos al correo

            $this->email->initialize($config);

            $this->email->from($post->email, 'Formulario de Contacto '.base_url());
            $this->email->to($contact_us->email);

            $this->email->subject('Contacto');

            $msg = $this->load->view('email_contact_us', $data, true);

            $this->email->message($msg);

            //Validate sendmail
            if( $this->email->send()){
                 $this->session->set_flashdata('success', 'Su mensaje a sido enviado');
                 redirect('contact_us');
            }else{
                $this->session->set_flashdata('error', 'Error Mailing, Contact the Webmaster');
                redirect('contact_us');
            }
        } else {

            $this->session->set_flashdata('error', validation_errors());
            redirect('contact_us');

        }

    }



}