<section class="title">

    <h4>Nuestro Equipo</h4>

</section>

<section class="item">

    <div class="content">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-team"><span>Nuevo</span></a></li>

            </ul>

            <div class="form_inputs" id="page-team">

                <?php echo form_open_multipart(site_url('admin/our_team/'), 'id="form-wysiwyg"'); ?>

                <div class="inline-form">

                    <fieldset>

                        <ul>

                            <li>

                                <label for="name">Imagen

                                    <small>

                                        - Imagen Permitidas gif | jpg | png | jpeg<br>

                                        - Tamaño Máximo 2 MB<br>

                                        - Ancho Máximo 252px<br>

                                        - Alto Máximo 170px

                                    </small>

                                </label>

                                <div class="input">

                                    <div class="btn-false">

                                        <div class="btn">Examinar</div>

                                        <?php echo form_upload('image', '', ' id="image"'); ?>

                                    </div>

                                </div>

                                <br class="clear">

                            </li>

                            <li>

                                <label for="name">Nombre <span>*</span></label>

                                <div class="input"><?php echo form_input('name', set_value('name'), 'class="dev-input-title"'); ?></div>

                            </li>
                            <li>

                                <label for="cargo">Cargo <span>*</span></label>

                                <div class="input"><?php echo form_input('cargo', set_value('cargo')); ?></div>

                            </li>

                         <li>

                                <label for="exp">Experiencia

                                    <span>*</span>

                                    <small class="counter-text"></small>

                                </label>

                                <div class="input"><?php echo form_textarea('exp', set_value('exp'),'class="dev-input-textarea limit-text" length="200"'); ?></div>

                            </li>
                            <li>

                                <label for="fun">Funciones

                                    <span>*</span>

                                    <small class="counter-text"></small>

                                </label>

                                <div class="input"><?php echo form_textarea('fun', set_value('fun'),'class="dev-input-textarea limit-text" length="200"'); ?></div>

                            </li>

                            

                        </ul>

                    </fieldset>

         <br class="clear">  

                    <div class="buttons float-right padding-top">

                        <?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel'))); ?>

                    </div>

                </div>

                <?php echo form_close(); ?>

            </div>



        </div>

    </div>

</section>