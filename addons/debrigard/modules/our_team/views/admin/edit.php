<section class="title">

    <h4>Nuestro Equipo</h4>

    <br>

    <small class="text-help">Los campos señalados con <span>*</span> son obligatorios.</small>

</section>



<section class="item">

    <div class="description">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-team"><span>Equipo</span></a></li>

            </ul>



            <div class="form_inputs" id="page-team">

                <?php echo form_open_multipart(site_url('admin/our_team/edit_team/'.(isset($datosForm) ? $datosForm->id : '')), 'id="form-wysiwyg"'); ?>

                <div class="inline-form">

                    <fieldset>

                        <ul>

                            <li>

                                <label for="name">Imagen

                                    <small>

                                        - Imagen Permitidas gif | jpg | png | jpeg<br>

                                        - Tamaño Máximo 2 MB<br>

                                        - Ancho Máximo 400px<br>

                                        - Alto Máximo 400px

                                    </small>

                                </label>

                                <div class="input">

                                 <?php if (!empty($datosForm->image)): ?>

                                    <div>

                                        <img src="<?php echo site_url($datosForm->image) ?>" width="298">

                                    </div>

                                <?php endif; ?>

                                <div class="btn-false">

                                    <div class="btn">Examinar</div>

                                    <?php echo form_upload('image', '', ' id="image"'); ?>

                                </div>

                            </div>

                            <br class="clear">

                        </li>

                        <li>

                            <label for="name">Nombre<span>*</span></label>

                            <div class="input"><?php echo form_input('name', (isset($datosForm->name)) ? $datosForm->name : set_value('name'), 'class="dev-input-title"'); ?></div>

                        </li>
                        <li>

                            <label for="cargo">Cargo<span>*</span></label>

                            <div class="input"><?php echo form_input('cargo', (isset($datosForm->cargo)) ? $datosForm->cargo : set_value('cargo')); ?></div>

                        </li>  

                        <li class="even">

                                 <label for="name">Experiencia<span>*</span></label>
                             <div class="input">
                            <div class="sroll-table">
                                <?php echo form_textarea(array('id' => 'exp', 'name' => 'exp', 'value' => (isset($datosForm->exp)) ? $datosForm->exp : set_value('exp'), 'rows' => 5)) ?>
                                <!--<input type="hidden" name="exp" id="text">-->
                            </div>
                        </div>
                        <br class="clear">

                            </li>

                            <li class="even">

                        <label for="name">Funciones<span>*</span></label>
                             <div class="input">
                            <div class="sroll-table">
                                <?php echo form_textarea(array('id' => 'fun', 'name' => 'fun', 'value' => (isset($datosForm->fun)) ? $datosForm->fun : set_value('fun'), 'rows' => 5)) ?>
                               <!-- <input type="hidden" name="fun" id="text">-->
                            </div>
                        </div>
                        <br class="clear">
              </li>
                       </ul>
                       <br class="clear">
                             <div class="buttons float-right padding-top">
                               <?php

                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));

                        ?>
         
                             </div>
                       </fieldset>
                         <?php echo form_close(); ?>
                       </div>
                     </div>
                 </div>
             </div>
    </section>


                                 
                              
      



              


