<section class="title">

    <h4>Nuestro Equipo</h4>

</section>

<section class="item">

    <div class="content">

        <div class="tabs">

            <ul class="tab-menu">

                <li><a href="#page-team"><span>Lista de Contenidos</span></a></li>

                

                

            </ul>



            <!-- PRODUCTOS -->

            <div class="form_inputs" id="page-team">

                <fieldset>



                    <?php echo anchor('admin/our_team/edit_team', '<span>+ Nuevo</span>', 'class="btn blue"'); ?>

                    <br>

                    <br>



                    <?php if (!empty($teams)): ?>



                        <table border="0" class="table-list" cellspacing="0">

                            <thead>

                                <tr>

                                    <th style="width: 15%"><span>Nombre</span></th>

                                    <th style="width: 10%">Cargo</th>

                                    <th style="width: 20%">Imagen</th>

                                    <th style="width: 20%">Funciones</th>

                                      <th style="width: 20%">Experiencia</th>

                                    <th style="width: 15%">Acciones</th>

                               

                                

                                

                                

                                </tr>

                            </thead>

                            <tfoot>

                                <tr>

                                    <td colspan="6">

                                        <div class="inner filtered"><?php $this->load->view('admin/partials/pagination') ?></div>

                                    </td>

                                </tr>

                            </tfoot>

                            <tbody>

                                <?php foreach ($teams as $team): ?>

                                    <tr>

                                         <td><?php echo substr(strip_tags($team->name), 0,100) ?></td>

                                           <td><?php echo substr(strip_tags($team->cargo), 0,100) ?></td>

                                       

                                        <td>

                                            <?php if (!empty($team->image)): ?>

                                                <img src="<?php echo site_url($team->image); ?>" style="height: 130px;">

                                            <?php endif; ?>

                                        </td>

                                        <td><?php echo substr(strip_tags($team->fun), 0,200) ?></td>
                                             <td><?php echo substr(strip_tags($team->exp), 0,200) ?></td>

                                        <td>

                                            <?php echo anchor('admin/our_team/edit_team/' . $team->id, lang('global:edit'), 'class="btn green small"'); ?>

                                    

                                            <?php echo anchor('admin/our_team/delete_team/' . $team->id, lang('global:delete'), array('class' => 'btn red small confirm button')) ?>                        

                                            
            

                                            

                                        </td>

                                    </tr>

                                <?php endforeach ?>

                            </tbody>

                        </table>



                    <?php else: ?>

                        <p style="text-align: center">No hay Registros actualmente</p>

                    <?php endif ?>

                </fieldset>

            </div>



            <!-- CATEGORIAS -->

           

</section>