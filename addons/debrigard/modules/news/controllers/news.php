<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author 	    Brayan Acebo
 * @author      Luis Fernando Salazar Buitrago
 * @package 	PyroCMS
 * @subpackage 	News
 * @category 	Modulos
 * @license 	Apache License v2.0
 */

class News extends Public_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('news_m');
    }

    // -----------------------------------------------------------------

    public function index($page = null)
    {
           
    	// Banner
        $banner = $this->news_m->get_all();

        $pagination = create_pagination('news/index', $this->news_m->count_all(), 6, 3);

        $news = $this->news_m
        ->limit($pagination['limit'], $pagination['offset'])
        ->order_by('position','ASC')
        ->get_all();

        foreach($news AS $item)
        {
            $item->title = substr($item->title, 0, 34);
            $item->image = val_image($item->image);
            $item->introduction = substr($item->introduction, 0, 600);
            $item->date = fecha_spanish_full($item->date);
            $item->urlDetail = site_url('news/detail/'.$item->slug);
        }

        $this->template
        ->set('news', $news)
        ->set('pagination', $pagination['links'])
        ->build('index');
    }

    // ----------------------------------------------------------------------

    public function detail($slug)
    {

        $data = $this->news_m->get_many_by('slug', $slug);

        if(!$data)
            redirect('news');

        $post = array();

        if (count($data) > 0) {
            $post = $data[0];
        }
        // Se convierten algunas variables necesarias para usar como slugs
        $setter = array(
            'image' => val_image($post->image),
            'date' => fecha_spanish_full($post->date)
            );

        $data_end = array_merge((array)$post,$setter);

        $this->template
        ->title($this->module_details['name'])
        ->set('data', (object) $data_end)
        ->build('detail');

    }
}