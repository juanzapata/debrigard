<?php



if (!defined('BASEPATH'))

	exit('No direct script access allowed');



/**

 *

 * @author 	Luis Fernando Salazar Buitrago

 * @author 	Brayan Acebo

 * @package 	PyroCMS

 * @subpackage 	News

 * @category 	Modules

 * @license 	Apache License v2.0

 */



class Admin extends Admin_Controller {



	public function __construct()

	{

		parent::__construct();

		$this->lang->load('news');

		$this->template

		->append_js('module::developer.js')

		->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));

		$this->load->model('news_m');

	}



	public function index()

	{

		// news

		$pagination = create_pagination('admin/news/index', $this->news_m->count_all(), 10);



		$news = $this->news_m

		->limit($pagination['limit'], $pagination['offset'])

		->order_by('position','ASC')

		->get_all();



		$pag = $pagination['offset'];



		$this->template

		->set('news', $news)

		->set('pagination', $pagination)

		->set('pag', $pag)

		->build('admin/new_back');

	}

public function estado($estado,$id){



        $this->load->model('news_m');

        $this->db->select('estado');

        $this->db->where('estado', 1);

        $this->db->from($this->db->dbprefix('news'));

        $query = $this->db->get();

        $solucion = $query->result();

       // $this->result = (object) $solucion;



        $count = count($solucion);

        if($count < 2){

        $archivos = array('estado'=> $estado );
        

        $this->news_m->update_estado($archivos,$id);

        redirect('admin/news');

        }else{   
		 $this->session->set_flashdata('error', 'Solo puede destacar 2 Noticias');
		
        $archivos = array(

       'estado'=> 0  

     
      

        );

        $this->news_m->update_estado($archivos,$id);

        redirect('admin/news');

        }



    }

	public function edit_new($idItem = null)

	{

		$this->form_validation->set_rules('title', 'Título', 'required|max_length[255]|trim');

		$this->form_validation->set_rules('content', 'Contenido', 'required|trim');

		$this->form_validation->set_rules('introduction', 'Introducción', 'required|max_length[600]|trim');



		if($this->form_validation->run()!==TRUE)  // abrimos el formulario de edicion

		{

			if(validation_errors() == "")

			{

				$this->session->set_flashdata('error', validation_errors());

			}

			if(!empty($idItem))  // si se envia un dato por la URL se hace lo siguiente (Edita)

			{

				$idItem or redirect('admin/news');



				$titulo = 'Editar Noticia';

				$datosForm = $this->news_m->get($idItem);



				$positionNews = $this->news_m

				->order_by('position', 'ASC')

				->get_all();



				$this->template

				->set('datosForm', $datosForm)

				->set('positionNews', $positionNews)

				->set('titulo', $titulo)

				->set('ban', true)

				->build('admin/edit_new_back');

			}

			else

			{

				$titulo = 'Crear noticia';



				$this->template

				->set('titulo', $titulo)

				->set('ban', false)

				->build('admin/edit_new_back');

			}

		}

		else // si el formulario ha sido enviado con éxito se procede

		{

			if($idItem != null)  // si se envia un dato por la URL se hace lo siguiente (Edita)

			{



				$post = (object) $this->input->post();

				$news = $this->news_m->get($post->position_new);



				$data = array(

					'title' => $post->title,

					'slug' => slug($post->title), // Creación de slug's para url's limpias

					'introduction' => $post->introduction,

					'content' => html_entity_decode($post->content),

					'position' => $news->position

					);



				$config['upload_path'] = './' . UPLOAD_PATH . '/news';

				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['max_size'] = 2050;

				$config['encrypt_name'] = true;



				$this->load->library('upload', $config);



	            // imagen uno

				$img = $_FILES['image']['name'];



				if (!empty($img)) {

					if ($this->upload->do_upload('image')) {

						$datos = array('upload_data' => $this->upload->data());

						$path = UPLOAD_PATH . 'news/' . $datos['upload_data']['file_name'];

						$img = array('image' => $path);

						$data = array_merge($data, $img);

						$obj = $this->db->where('id', $idItem)->get('news')->row();

						@unlink($obj->image);

					} else {

						$this->session->set_flashdata('error', $this->upload->display_errors());

						redirect('admin/news/');

					}

				}



				if ($this->news_m->update($idItem, $data)) {

					// Se actualiza el Orden

					$position = array('position' => $post->position_current);

					$this->news_m->update($news->id, $position);



					$this->session->set_flashdata('success', 'Los registros se actualizarón con éxito.');

					redirect('admin/news/');

				} else {

					$this->session->set_flashdata('success', lang('home:error_message'));

					redirect('admin/news/');

				}

			}

			else

			{

				$post = (object) $this->input->post();



				$this->db->select_max('position');

				$query = $this->db->get('news');

				$position = $query->row();



				$data = array(

					'title' => $post->title,

					'slug' => slug($post->title), // Creación de slug's para url's limpias

					'introduction' => $post->introduction,

					'content' => html_entity_decode($post->content),

					'date' => date("Y-m-d H:i:s"),

					'position' => $position->position + 1

					);



				$config['upload_path'] = './' . UPLOAD_PATH . '/home_banner';

				$config['allowed_types'] = 'gif|jpg|png|jpeg';

				$config['max_size'] = 2050;

				$config['encrypt_name'] = true;



				$this->load->library('upload', $config);



	            // imagen uno

				$img = $_FILES['image']['name'];



				if (!empty($img)) {

					if ($this->upload->do_upload('image')) {

						$datos = array('upload_data' => $this->upload->data());

						$path = UPLOAD_PATH . 'home_banner/' . $datos['upload_data']['file_name'];

						$img = array('image' => $path);

						$data = array_merge($data, $img);

					} else {

						$this->session->set_flashdata('error', $this->upload->display_errors());

						redirect('admin/news/');

					}

				}



				if ($this->news_m->insert($data)) {

					$this->session->set_flashdata('success', 'Los registros se ingresaron con éxito.');

				} else {

					$this->session->set_flashdata('success', lang('home:error_message'));

				}

				redirect('admin/news');

			}

		}



	}



	public function delete_new($id = null) {



		$id or redirect('admin/news');



		$obj = $this->db->where('id', $id)->get($this->db->dbprefix.'news')->row();



		if ($this->news_m->delete($id)) {

			@unlink($obj->image);

			$this->session->set_flashdata('success', 'El registro se elimino con éxito.');

		} else {

			$this->session->set_flashdata('error', 'No se logro eliminar el registro, inténtelo nuevamente');

		}

		redirect('admin/news');

	}



}

