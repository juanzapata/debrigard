<?php $cont = 0; ?>
<div class="container">

	<!-- CARROUSEL -->
	<div id="carousel-example-generic" class="carousel slide " data-ride="carousel" style="max-width: 1080px;">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<?php
			$targets_count = count($banner);
			for($i = 0; $i < $targets_count; $i++):?>
			<li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" <?php echo ($i == 0 ? 'class="active"' : '');?>></li>
		<?php endfor;?>
	</ol>

	<!-- Wrapper for slides -->
 
	<div class="carousel-inner">
		<?php
		$primero = false;
		foreach ($banner as $item):
			?>
		<div class="item <?php echo (!$primero ? 'active' : '') ?>">
			<a href="<?php echo $item->link ?>"><img src="<?php echo site_url($item->image); ?>" alt="<?php echo $item->title; ?>" ></a>
			<div class="carousel-caption">
				<!--<h3><?php echo $item->title; ?></h3>
				<p class="hidden-xs"><?php echo $item->text; ?></p>-->
			</div>
		</div>
		<?php $primero = true; endforeach; ?>
	</div>


	<!-- Controls -->
	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	</a>
	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	</a>
</div>
<div class="push"></div>
<div>
<?php foreach ($informative_text as $t): ?>
		<div class="col-sm-6 col-md-3">			
				
					<h4><?php echo substr($t->title, 0,45) ?></h4>
					<h4><?php echo ($t->texto) ?></h4>	
				
			</div>
		</div>
	<?php endforeach ?>
	
<div class="push"></div>
<hr>
<h4>Noticias o Publicaciones Destacadas</h4>
<!-- Noticias Destacadas -->
{{ widgets:area slug="mapas" }}

<h4>Productos o Servicios destacados</h4>

<div class="push"></div>
<hr>
<h4>Nuestros Clientes</h4>
<!-- Nuestrosn clientes -->
<div class="row">	
	<?php foreach ($outstanding_services as $service): ?>
		<div class="col-sm-6 col-md-3">
			<div class="thumbnail">
				<div style="overflow: hidden;max-height:170px;">
				<a href="<?php echo $service->link ?>" target="_blank"><img src="<?php echo site_url($service->image); ?>" alt="<?php echo $service->title ?>" data-src="holder.js/300x200" class="img-responsive" style="min-width: 100%;" ></a>
				</div>
				<div class="caption">
					<!--<h4><?php echo substr($service->title, 0,45) ?></h4>-->		
				</div>
			</div>
		</div>
	<?php endforeach ?>
</div>

<div class="push"></div>

<div class="push"></div>

</div>