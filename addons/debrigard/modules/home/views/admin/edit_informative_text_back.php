<section class="item">
    <div class="content">
    	<h2>Home / Texto informativo</h2>
        <div class="tabs">
            <ul class="tab-menu">
                <li><a href="#page-texts"><span><?php echo $titulo; ?></span></a></li>
            </ul>


            <div class="form_inputs" id="page-bibliografia">
            <?php echo form_open_multipart(site_url('admin/home/edit_informative_text/'.(isset($informative) ? $informative->id : '')), 'class="crud"'); ?>
            <div class="inline-form">
          <fieldset>
                    <ul>                        
                            <li>
                            <label for="name">Titulo <span>*</span></label>
                            <div class="input"><?php echo form_input('title', (isset($informative->title)) ? $informative->title : set_value('title'), 'style="width: 350px;"'); ?></div>
                        </li>
                        <li>
                            <label for="name">Texto <span>*</span></label>
                            <div class="input"><?php echo form_input('text', (isset($informative->text)) ? $informative->text : set_value('text'), 'style="width: 500px;height:200px;"'); ?></div>
                        </li>
                        
                    </ul>
                </fieldset>

                <?php 
                        if(isset($informative))
                        {
                            echo form_hidden('id', $informative->id);
                        }
                        $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'cancel')));
                    ?>
            </div>
                <?php echo form_close(); ?>
            </div>

        </div>
    </div>
</section>