<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * @author 	    Brayan Acebo
 * @subpackage 	Sobre Nosotros Module
 * @category 	Modulos
 * @license 	Apache License v2.2
 */
class Admin extends Admin_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('footer');
        $this->template
                ->append_js('module::developer.js')
                ->append_metadata($this->load->view('fragments/wysiwyg', null, TRUE));
        $models = array(
            'footer_model'
        );
        $this->load->model($models);
    }

    // ------------------------------------------------------------------------------------------

    public function index() {


        $result = $this->footer_model->get_all();

        $funcion = 'create';
        $post = array();
        if (count($result) > 0) {
            $funcion = 'edit';
            $post = $result[0];
        }

        $pagination = create_pagination('admin/footer/edit/', $this->footer_model->count_all(), 10);


        $this->template->set('funcion', $funcion)
                ->set('data', $post)
                ->set('pagination', $pagination)
                ->build('admin/footer');
    }

    public function create() {
        $data = array(
            'nombre' => $this->input->post('nombre'),
            'telephone' => $this->input->post('telephone'),
            'address' => $this->input->post('address'),
            'email' => $this->input->post('email'),
            'city' => $this->input->post('city')
        );

        

        if ($this->footer_model->insert($data)) {
            // insert ok, so
            $this->session->set_flashdata('success', lang('footer:success_message'));
            redirect('admin/footer/');
        } else {
            $this->session->set_flashdata('error', lang('footer:error_message'));
            redirect('admin/footer/');
        }

        $this->template->set('funcion', 'create')
                ->build('admin/footer');
    }

    public function edit($idItem = null) {
        $data = array(
            'nombre' => $this->input->post('nombre'),
            'telephone' => $this->input->post('telephone'),
            'address' => $this->input->post('address'),
            'email' => $this->input->post('email'),
            'city' => $this->input->post('city')
        );

        $config['upload_path'] = './' . UPLOAD_PATH . '/footer';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 2050;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        // imagen uno
        
        $img = $_FILES['image']['name'];
        if (!empty($img)) {
            if ($this->upload->do_upload('image')) {
                $datos = array('upload_data' => $this->upload->data());
                $path = UPLOAD_PATH . 'footer/' . $datos['upload_data']['file_name'];
                $img = array('image' => $path);
                $data = array_merge($data, $img);
                $obj = $this->db->where('id', $idItem)->get('footer')->row();
                @unlink($obj->image);
            } else {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('admin/footer/');
            }
        }

        if ($this->footer_model->update_all($data)) {

            // insert ok, so
            $this->session->set_flashdata('success', lang('contact_us:success_message'));
            redirect('admin/footer/');
        } else {
            $this->session->set_flashdata('error', lang('contact_us:error_message'));
            redirect('admin/footer/');
        }

        $this->template->set('funcion', 'edit')
                ->build('admin/footer');
    }

}
